# compilateur utilisé
CC = g++

# dossiers du projet
SRCDIR  = src
LIBDIR  = obj
BINDIR  = bin
TESTDIR = tests

# options du compilateur
CFLAGS = -O2 -Wall -Wextra -std=c++11 -g
LIBS   = -larmadillo

# création des listes de fichiers utiles
SRC   = $(wildcard $(SRCDIR)/*.cpp)
HEADS = $(wildcard $(SRCDIR)/*.h)
OBJ   = $(SRC:$(SRCDIR)/%.cpp=$(LIBDIR)/%.o)
TEST  = $(wildcard $(TESTDIR)/test_*.h)

# compilation du projet entier + librairie statique
projet: $(OBJ)
	ar rvs $(LIBDIR)/lib.a $(OBJ)
	$(CC) $(CFLAGS) -o $(BINDIR)/projet $(LIBDIR)/lib.a $(LIBS)

# compilation en fichiers objet
$(LIBDIR)/%.o: $(SRCDIR)/%.cpp
	$(CC) $(CFLAGS) -o $@ -c $<

# build et lance les tests unitaires
test:
	cxxtestgen --error-printer -o $(TESTDIR)/tests.cpp $(TEST)
	$(CC) $(CFLAGS) -o $(BINDIR)/tests $(TESTDIR)/tests.cpp $(LIBDIR)/lib.a
	./$(BINDIR)/tests

# nettoie tous les fichiers générés
clean:
	rm -rf $(LIBDIR)/*.o
	rm -rf $(LIBDIR)/lib.a
	rm -rf $(BINDIR)/projet
	rm -rf $(BINDIR)/tests
	rm -rf $(TESTDIR)/tests.cpp
