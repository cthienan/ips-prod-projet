/**
 * \file poly.h
 * Interface contenant la classe Poly, modélisant les polynômes.
 */

#ifndef POLY_H
#define POLY_H

#include <armadillo>

/**
 * \brief Classe contenant les différents polynômes utilisés.
 * \details Cette classe est utilisée pour calculer les polynômes d'Hermite en un certain point z.
 * \author Thien An CAO
 * \author Mathis MARTIN
 */
class Poly
{
private:
    arma::mat matHermite;
    arma::cube matLaguerre;

public:
    Poly();
    arma::vec hermite(long long int);
    arma::vec laguerre(long long int,long long int);
    void calcHermite(long long int,arma::vec);
    void calcLaguerre(long long int,long long int,arma::vec);
};

#endif // POLY_H