/**
 * \file utils.h
 * Conteneur de constantes et fonctions utilitaires
 */

#ifndef UTILS_H
#define UTILS_H

/**
 * 
 */
long long int fact(long long int n)
{
    if (n == 0)
        return 1;
    return n * fact(n - 1);
}

#endif // UTILS_H