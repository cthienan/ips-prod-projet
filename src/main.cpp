/**
 * \mainpage Projet IPS-PROD
 *
 * \section Introduction
 * Ceci est la documentation du projet IPS-PROD de Thien An CAO et Mathis MARTIN.
 * \section Objectifs
 * - Calculer la densité de probabilité qu'une particule se trouve à un endroit donné de l'espace
 * - Optimiser le calcul pour un speedup maximal
 * - Faire un rendu 3D avec POV-RAY
 * \section Installation
 * - Etape 1:  Lancez un terminal depuis le dossier du projet
 * - Etape 2:  Tapez la commande `make`
 * - Etape 3:  Tapez dans le terminal `./bin/projet 0` pour un bench des optimisations
 * - Etape 4:  Tapez `./run.sh $W $H` pour un rendu en taille W x H
 * - Etape 5:  Enjoy!
 */

/**
 * \file main.cpp
 * \author Thien-An CAO
 * \author Mathis MARTIN
 */

// includes standards
#include <stdio.h>
#include <cstring>
#include <iostream>
#include <cmath>
#include <time.h>

// includes locaux
#include "basis.h"

/**
 * df3 writer pour POV-RAY
 */
std::string cubeToDf3(const arma::cube &m)
{
    std::stringstream ss(std::stringstream::out | std::stringstream::binary);
    int nx = m.n_rows;
    int ny = m.n_cols;
    int nz = m.n_slices;

    ss.put(nx >> 8);
    ss.put(nx & 0xff);
    ss.put(ny >> 8);
    ss.put(ny & 0xff);
    ss.put(nz >> 8);
    ss.put(nz & 0xff);
    
    double theMin = 0.0;
    double theMax = m.max();

    for (uint k = 0; k < m.n_slices; k++){
        for (uint j = 0; j < m.n_cols; j++){
            for (uint i = 0; i < m.n_rows; i++){

                uint v = 255 * (fabs(m(i, j, k)) - theMin) / (theMax - theMin);
                ss.put(v);
            }
        }
    }

    return ss.str();
}

/**
 * Fonction main
 * Demande à l'utilisateur de rentrer des valeurs et retourne les résultats de l'oscillateur harmonique quantique correspondant
 * argument ligne de commande : 0 -> speedup test, 1 -> algo naif, 2 -> opti 1...
 */
int main (int argc, char** argv)
{
    // default : performance comparison mode
    int mode = 0;

    // parsing de l'argument en ligne de commande
    if(argc > 1)
    {
        char *p;

        errno = 0;
        long conv = strtol(argv[1], &p, 10);

        // Check for errors: e.g., the string does not represent an integer
        // or the integer is larger than int
        if (errno != 0 || *p != '\0' || conv > INT_MAX || conv < INT_MIN) {
            // Put here the handling of the error, like exiting the program with
            // an error message
        } else {
            // No error
            mode = conv;
            //printf("%d\n", mode);
        }
    }

    Basis basis(1.935801664793151, 2.829683956491218, 14, 1.3);
    arma::mat rho;

    int nbR = 32;
    int nbY = 32;
    int nbZ = 64;

    arma::vec rVals    = arma::linspace(-10,10, nbR);
    arma::vec zVals    = arma::linspace(-20,20, nbZ);
    arma::vec rValsSym = arma::linspace(  0,10, nbR/2);
    rho.load("./src/rho.arma", arma::arma_ascii);

    arma::mat  result1 = arma::zeros(nbR,   nbZ);
    arma::mat  result2 = arma::zeros(nbR,   nbZ);
    arma::mat  result3 = arma::zeros(nbR/2, nbZ); // symmetry, thus size/2
    arma::cube res3D   = arma::zeros(nbR,   nbY, nbZ);

    // buffer variables defined here for perf boost
    arma::mat funcA     = arma::zeros(nbR,   nbZ);
    arma::mat funcB     = arma::zeros(nbR,   nbZ);

    arma::vec zPartA    = arma::zeros(nbZ);
    arma::vec rPartA    = arma::zeros(nbR);
    arma::vec zPartB    = arma::zeros(nbZ);
    arma::vec rPartB    = arma::zeros(nbR);

    // special sizes for symmetry
    arma::mat funcASym  = arma::zeros(nbR/2, nbZ);
    arma::mat funcBSym  = arma::zeros(nbR/2, nbZ);
    arma::vec rPartASym = arma::zeros(nbR/2);
    arma::vec rPartBSym = arma::zeros(nbR/2);

    uint i   = 0;
    uint j   = 0;
    uint inc = 0;

    switch(mode)
    {
        case 0:
            // Perf comparison mode
            struct timespec t0, t1, t2, t3;
            double inter1, inter2, inter3;

            // naive algoritm

            // Get t0
            clock_gettime(CLOCK_REALTIME, &t0);

            for (int m1 = 0; m1 < basis.mMax; m1++)
            {
                for (int n1 = 0; n1 < basis.nMax(m1); n1++)
                {
                    for (int n_z1 = 0; n_z1 < basis.n_zMax(m1, n1); n_z1++)
                    {
                        j = 0;
                        for (int m2 = 0; m2 < basis.mMax; m2++)
                        {
                            for (int n2 = 0; n2 < basis.nMax(m2); n2++)
                            {
                                for (int n_z2 = 0; n_z2 < basis.n_zMax(m2, n2); n_z2++)
                                {
                                    // mat += mat % mat * double
                                    result1 += (basis.rPart(rVals, m1, n1) * basis.zPart(zVals, n_z1).t())
                                             % (basis.rPart(rVals, m2, n2) * basis.zPart(zVals, n_z2).t())
                                             * rho(i,j);

                                    j++;
                                }
                            }
                        }
                        i++;
                    }
                }
            }

            // Opti 1 : m1 = m2 = m

            // Get t1
            clock_gettime(CLOCK_REALTIME, &t1);

            i = 0;
            j = 0;
            for (int m = 0; m < basis.mMax; m++)
            {
                for (int n1 = 0; n1 < basis.nMax(m); n1++)
                {
                    rPartA = basis.rPart(rVals, m, n1);
                    for (int n_z1 = 0; n_z1 < basis.n_zMax(m, n1); n_z1++)
                    {
                        zPartA = basis.zPart(zVals, n_z1);
                        funcA = rPartA * zPartA.t();
                        for (int n2 = 0; n2 < basis.nMax(m); n2++)
                        {
                            rPartB = basis.rPart(rVals, m, n2);
                            for (int n_z2 = 0; n_z2 < basis.n_zMax(m, n2); n_z2++)
                            {   
                                zPartB = basis.zPart(zVals, n_z2);
                                funcB = rPartB * zPartB.t();

                                result2 += funcA % funcB * rho(i,j);
                                j++;
                            }
                        }
                        i++;
                        if (i == j)
                            inc = i;
                        j = inc;
                    }
                }
            }

            // Get t2
            clock_gettime(CLOCK_REALTIME, &t2);

            // Opti 2 : symmétrie sur l'axe X
            i   = 0;
            j   = 0;
            inc = 0;
            for (int m = 0; m < basis.mMax; m++)
            {
                for (int n1 = 0; n1 < basis.nMax(m); n1++)
                {
                    rPartASym = basis.rPart(rValsSym, m, n1);
                    for (int n_z1 = 0; n_z1 < basis.n_zMax(m, n1); n_z1++)
                    {
                        zPartA = basis.zPart(zVals, n_z1);
                        funcA = rPartASym * zPartA.t();
                        for (int n2 = 0; n2 < basis.nMax(m); n2++)
                        {
                            rPartBSym = basis.rPart(rValsSym, m, n2);
                            for (int n_z2 = 0; n_z2 < basis.n_zMax(m, n2); n_z2++)
                            {   
                                zPartB = basis.zPart(zVals, n_z2);
                                funcB = rPartBSym * zPartB.t();

                                result3 += funcA % funcB * rho(i,j); // mat += mat % mat * double
                                j++;
                            }
                        }
                        i++;
                        if (i == j)
                            inc = i;
                        j = inc;
                    }
                }
            }

            // Get t3
            clock_gettime(CLOCK_REALTIME, &t3);

            // End
            inter1 = 1000.*(t1.tv_sec-t0.tv_sec) + (t1.tv_nsec-t0.tv_nsec)/1000000.;
            inter2 = 1000.*(t2.tv_sec-t1.tv_sec) + (t2.tv_nsec-t1.tv_nsec)/1000000.;
            inter3 = 1000.*(t3.tv_sec-t2.tv_sec) + (t3.tv_nsec-t2.tv_nsec)/1000000.;

            printf("\nAlgorithme naïf : %lf ms\n", inter1);

            printf("\nm1 = m2 = m     : %lf ms\n", inter2);
            printf("   - Speedup : %lf\n", inter1/inter2);

            printf("\nSymmétrie sur X : %lf ms\n", inter3);
            printf("   - Speedup : %lf\n", inter2/inter3);
            printf("   - Cumulé  : %lf\n", inter1/inter3);

            printf("\n");
            break;
        
        case 1:

            // algorithme naïf
            for (int m1 = 0; m1 < basis.mMax; m1++)
            {
                for (int n1 = 0; n1 < basis.nMax(m1); n1++)
                {
                    for (int n_z1 = 0; n_z1 < basis.n_zMax(m1, n1); n_z1++)
                    {
                        j = 0;
                        for (int m2 = 0; m2 < basis.mMax; m2++)
                        {
                            for (int n2 = 0; n2 < basis.nMax(m2); n2++)
                            {
                                for (int n_z2 = 0; n_z2 < basis.n_zMax(m2, n2); n_z2++)
                                {
                                    // mat += mat % mat * double
                                    result1 += (basis.rPart(rVals, m1, n1) * basis.zPart(zVals, n_z1).t())
                                             % (basis.rPart(rVals, m2, n2) * basis.zPart(zVals, n_z2).t())
                                             * rho(i,j);

                                    j++;
                                }
                            }
                        }
                        i++;
                    }
                }
            }

            break;
        
        case 2:

            // Opti 1 : m1 = m2 = m
            for (int m = 0; m < basis.mMax; m++)
            {
                for (int n1 = 0; n1 < basis.nMax(m); n1++)
                {
                    rPartA = basis.rPart(rVals, m, n1);
                    for (int n_z1 = 0; n_z1 < basis.n_zMax(m, n1); n_z1++)
                    {
                        zPartA = basis.zPart(zVals, n_z1);
                        funcA = rPartA * zPartA.t();
                        for (int n2 = 0; n2 < basis.nMax(m); n2++)
                        {
                            rPartB = basis.rPart(rVals, m, n2);
                            for (int n_z2 = 0; n_z2 < basis.n_zMax(m, n2); n_z2++)
                            {   
                                zPartB = basis.zPart(zVals, n_z2);
                                funcB = rPartB * zPartB.t();

                                result1 += funcA % funcB * rho(i, j); // mat += mat % mat * double
                                j++;
                            }
                        }
                        i++;
                        if (i == j)
                            inc = i;
                        j = inc;
                    }
                }
            }

            break;
        
        case 3:

            // Opti 2 : symmétrie sur l'axe X
            for (int m = 0; m < basis.mMax; m++)
            {
                for (int n1 = 0; n1 < basis.nMax(m); n1++)
                {
                    rPartASym = basis.rPart(rValsSym, m, n1);
                    for (int n_z1 = 0; n_z1 < basis.n_zMax(m, n1); n_z1++)
                    {
                        zPartA = basis.zPart(zVals, n_z1);
                        funcA = rPartASym * zPartA.t();
                        for (int n2 = 0; n2 < basis.nMax(m); n2++)
                        {
                            rPartBSym = basis.rPart(rValsSym, m, n2);
                            for (int n_z2 = 0; n_z2 < basis.n_zMax(m, n2); n_z2++)
                            {   
                                zPartB = basis.zPart(zVals, n_z2);
                                funcB = rPartBSym * zPartB.t();

                                result3 += funcA % funcB * rho(i, j); // mat += mat % mat * double
                                j++;
                            }
                        }
                        i++;
                        if (i == j)
                            inc = i;
                        j = inc;
                    }
                }
            }

            // on copie ici dans une matrice nbR x nbZ pour ne pas avoir à
            // différencier les cas au replissage du cube, mais c'est superflu
            for(int k = 0 ; k < (nbR/2 + nbR%2) ; k++)
            {
                result1.row(k + nbR/2) = result3.row(k);
            }

            break;
    }

    if(mode > 0)
    {
        /**
        * Il est temps de peupler le cube !
        * On va itérer sur chaque point (x,y,z) du cube et y attribuer la valeur
        * correpondant aux coordonées (sqrt(x^2,z^2),y) dans le résultat précédant
        *
        * On fait tourner la figure 2D autour de l'axe z qui passe en sons centre,
        * il ne faut donc pas prendre x et y comme coordonnées cartésiennes, mais
        * x-nbR/2 et y-nbH/2, et on ajoute nbR/2 au résultat pour le redécaler.
        *
        * On n'oubliera pas de n'affcter la valeur conrrespondante que si le r
        * obtenu reste dans l'intervale [0, nbR-1]
        */
        int xx;
        int yy;
        int r;

        for(int x = 0 ; x < nbR ; x++){
            for(int y = 0 ; y < nbY ; y++){
                xx = x - nbR/2;
                yy = y - nbR/2;
                r = floor(sqrt( pow((double)xx, 2.) + pow((double)yy, 2.) ) + nbR/2);

                if((r >= 0) && (r < nbR))
                    res3D.tube(x, y) = result1.row(r);
            }
        }

        std::cout << cubeToDf3(res3D);
    }

    return 0;
}