/**
 * \file poly.cpp
 * Implémentation des méthodes de la classe Poly.
 * \author Thien-An CAO
 */

#include <stdio.h>
#include "poly.h"

/**
 * \brief Constructeur de la classe Poly.
 * \details Constructeur pour la classe Poly. Elle ne sert à rien car seule la fonction
 * recurrence nous intéresse. Le constructeur est donc vide.
 */
Poly::Poly()
{
}

/**
 * \brief Méthode de récurrence des polynômes d'Hermite.
 * \details Cette méthode permet de calculer les n premiers polynômes d'Hermite en z
 * par la formule de récurrence qui définit le polynôme d'Hermite puis garde les résultats
 * dans l'attribut matHermite de la classe. 
 *
 * \param n entier représentant l'indice du polynôme.
 * \param z vecteur contenant tous les points à appliquer au polynôme
 */
arma::vec Poly::hermite(long long int n)
{
    return this->matHermite.col(n);
}

/**
 * \brief Méthode de récurrence des polynômes d'Hermite.
 * \details Cette méthode permet de calculer les n premiers polynômes d'Hermite en z
 * par la formule de récurrence qui définit le polynôme d'Hermite puis garde les résultats
 * dans l'attribut matHermite de la classe. 
 *
 * \param n entier représentant l'indice du polynôme.
 * \param z vecteur contenant tous les points à appliquer au polynôme
 */
arma::vec Poly::laguerre(long long int m, long long int n)
{
    return this->matLaguerre.slice(n).col(m);
}

/**
 * \brief Méthode de récurrence des polynômes d'Hermite.
 * \details Cette méthode permet de calculer les n premiers polynômes d'Hermite en z
 * par la formule de récurrence qui définit le polynôme d'Hermite puis garde les résultats
 * dans l'attribut matHermite de la classe. 
 *
 * \param n entier représentant l'indice du polynôme.
 * \param z vecteur contenant tous les points à appliquer au polynôme
 */
void Poly::calcHermite(long long int n, arma::vec z)
{
    arma::vec h_0;
    h_0.ones(size(z)); ///< Génération d'un vecteur rempli de zéros de la taille de z

    for (int i = 0 ; i < n ; i++)
    {
        if (i == 0) ///< Cas n = 0
            this->matHermite.insert_cols(0, h_0);
        else if (i == 1) ///< Cas n = 1
            this->matHermite.insert_cols(1, 2*z);
        else
            this->matHermite.insert_cols(i, 2*z%this->matHermite.col(i-1) - 2*(i-1)*this->matHermite.col(i-2));
    }
}

/**
 * \brief Méthode de récurrence des polynômes de Laguerre.
 * \details Cette méthode permet de calculer les nu*m premiers polynômes de Laguerre en z
 * par la formule de récurrence qui définit le polynôme de Laguerre puis garde les résultats
 * dans l'attribut matLaguerre de la classe. 
 *
 * \param m entier représentant le premier nombre quantique dont dépend le polynôme.
 * \param n entier représentant le deuxième nombre quantique dont dépend le polynôme.
 * \param z vecteur contenant tous les points à appliquer au polynôme
 */
void Poly::calcLaguerre(long long int m, long long int n, arma::vec z)
{
    this->matLaguerre = arma::cube(z.n_elem, m+1, n+1);
    arma::mat L_0 = arma::ones(z.n_elem, m+1); ///< Génération d'une matrice remplie de zéros de la taille de z

    int i, j;

    for (i = 0 ; i < n ; i++)
    {
        if (i == 0)
        { ///< Cas n = 0
            this->matLaguerre.slice(0) = L_0;
        }
        else if (i == 1) ///< Cas n = 1
        {
            arma::mat L_1(z.n_elem, m+1);
            for (j = 0; j < m; j++)
            {
                L_1.col(j) = 1 + j - z;
            }
            this->matLaguerre.slice(1) = L_1;
        }
        else
        {
            arma::mat L(z.n_elem, m+1);
            
            for (j = 0; j < m ; j++)
            {
            L.col(j) = (2.0 + (j - 1 - z) / i)%this->matLaguerre.slice(i-1).col(j) - (1 + ((double)(j-1)) / i)*this->matLaguerre.slice(i-2).col(j);
            }
            this->matLaguerre.slice(i) = L;
            L.reset();
        }
    }
}