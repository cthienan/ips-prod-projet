/**
 * \file basis.h
 * Interface contenant la classe Basis, calculant les troncatures de bases.
 */

#ifndef BASIS_H
#define BASIS_H

#include <armadillo>

/**
 * \brief Classe calculant les troncatures de bases.
 * \details Cette classe est utilisée pour calculer les troncatures de bases à partir des paramètres br, bz, N et Q.
 * \author Thien An CAO
 * \author Mathis MARTIN
 */
class Basis
{
public:

    // methodes
    Basis(double br, double bz, int N, double Q);
    arma::vec rPart(arma::vec z, long long int m, long long int n);
    arma::vec zPart(arma::vec r, long long int n_z);
    
    // attributs
    double br;
    double bz;
    long long int mMax;
    arma::ivec nMax;
    arma::imat n_zMax;
};

#endif // HERMITE_H