/**
 * \file basis.cpp
 * Implémentation de la classe Basis.
 */

#include <math.h>
#include "basis.h"
#include "poly.h"
#include "utils.h"


/**
 * \brief Calcul intermédiaire de nz_max servant pour nMax
 * \param i l'entier en entrée
 * \param N
 * \param Q
 * \return un double égal à (N+2) * Q^(3/2) + 1/2 - i*Q
 */
static double nz_max(int i, int N, double Q)
{
    return (N + 2) * pow(Q, 2.0 / 3.0) + 0.5 - i * Q;
}

/**
 * \brief Constructeur de la classe Basis
 * \details Le constructeur va calculer les valeurs de mMax, nMax et n_zMax en
 * fonction des paramètres
 * \param br le facteur de déformation 'b perpendiculaire' de la base
 * \param bz le facteur de déformation 'bz' de la base
 * \param N
 * \param Q
 */
Basis::Basis(double br, double bz, int N, double Q)
{
    /*
    printf("\n\n ____________________________\n");
    printf(    "|                            |\n");
    printf(    "|  Basis Constructor called  |\n");
    printf(    "|____________________________|\n");
    */

    this->br = br;
    this->bz = bz;

    // on va avoir besoin de ces petits compteurs ^^
    long long int i = 0;
    long long int j = 0;

    /**
     * Calcul de mMax = sup{i : nz_max(i) >= 1}, soit le dernier entier i tel
     * que nz_max(i) >= 1
     */
    // printf("\n>>> mMax\n");
    
    double m_max = 0.0;

    while (nz_max(i, N, Q) >= 1)
    {
        // printf("nz_max = %f\n", nz_max(i, N, Q));
        m_max = i;
        i++;
    };
    // printf("nz_max = %f\n", nz_max(i, N, Q));

    // mMax est alors la partie entière inférieure de m_max
    this->mMax = floor(m_max);
    // printf("mMax = %lld\n", this->mMax);

    /**
     * Calcul des nMax
     */
    // printf("\n>>> nMax\n");
    
    // on redimensionne à la bonne taille, à savoir le range des valeurs de m
    (this->nMax) = arma::ivec(this->mMax);
    this->nMax.zeros();

    for ( i = 0 ; i < this->mMax ; i++ )
    {
        this->nMax(i) = floor(0.5 * (m_max - i - 1) + 1);
        // printf("nMax[%*lld] = %lld\n", (int)floor(log2(this->mMax) - 1), i, this->nMax(i));
    }

    /**
     * Calcul des n_zMax
     */
    // printf("\n>>> n_zMax\n");
    
    // idem, on aura une taille max(nMax) et on ajoutera mMax lignes
    long long int width = max(nMax);

    // printf("n_zMax dimensions : %lli x %lli\n", mMax, width);
    
    (this->n_zMax) = arma::imat(mMax, width);
    this->n_zMax.zeros();
    
    for ( i = 0 ; i < mMax ; i++ )
    {
        arma::Row<long long int> nzmax_i(width);
        nzmax_i.zeros();

        for ( j = 0 ; j < width ; j++ )
        {
            nzmax_i(j) = std::max((long long int)0, (long long int)floor(nz_max(i + 2 * j + 1, N, Q)));
        }

        this->n_zMax.row(i) = nzmax_i;
    }

    // printf("\n");
    // this->n_zMax.print();
}


/**
 * \brief calcule le terme Z(z,nz) de la fonction de base
 * \param z
 * \param nz
 */
arma::vec Basis::zPart(arma::vec z, long long int nz)
{
    Poly pol;
    pol.calcHermite(nz + 1, z / (double)(this->bz));

    return
        1 / (double)( pow(this->bz, 0.5)
            * pow(pow(2, nz)
            * pow(M_PI, 0.5)
            * (double)(fact(nz)), 0.5) )
        * arma::exp(- 0.5 * arma::pow(z / (double)(this->bz), 2)) % pol.hermite(nz);
}


/**
 * \brief calcule le terme R(r, m, n) de la fonciton de base
 * \param r
 * \param m
 * \param n
 */
arma::vec Basis::rPart(arma::vec r, long long int m, long long int n)
{
    Poly pol;
    pol.calcLaguerre(m+1, n+1, arma::pow(r / (double)(this->br), 2));
    return
        1 / (double)(this->br * pow(M_PI, 0.5)) * pow(fact(n) / (double)fact(n + abs(m)), 0.5)
        * arma::exp(- 0.5 * arma::pow(r / this->br, 2))
        % arma::pow(r / this->br, m) % pol.laguerre(m, n);
}