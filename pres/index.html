<!DOCTYPE html>
<html>
  <head>
    <title>Projet IPS-PROD : Presentation</title>
    <meta charset="utf-8" />
    <link rel="stylesheet" type="text/css" href="core/fonts/mono.css"> 
    <link rel="stylesheet" type="text/css" href="core/css/animate.css"> 
    <link rel="stylesheet" type="text/css" href="core/css/style_core.css"> 
    <link rel="stylesheet" type="text/css" href="core/css/mermaid.css"> 
    <link rel="stylesheet" type="text/css" href="core/css/gitgraph.css"> 
    <link rel="stylesheet" type="text/css" href="core/css/style_ensiie.css"> 
    <link rel="stylesheet" type="text/css" href="core/css/katex.css"> 
    <link rel="stylesheet" type="text/css" href="core/css/asciinema-player.css"> 



  </head>
  <body>
    <textarea id="source" readonly>

class: titlepage

.title[Projet d'Introduction à la Programmation Scientifique]

.subtitle[Densité locale d'un système nucléaire]

.footnote[T.A. Cao, M. Martin - ENSIIE - 2021]

---

layout: true
class: animated fadeIn middle numbers

.footnote[Projet IPS-PROD - T.A. Cao, M. Martin - ENSIIE - 2021]

---

# Contexte

## Problème physique

Calcul de la densité locale d'un système nucléaire :

`$$\rho(\mathbf{r}) \equiv \sum_a \sum_b \rho_{ab}\psi_a(\mathbf{r})\psi^*_b(\mathbf{r})$$`

où 

* `${\rho_{ab}}$` est la matrice de **densité complexe**
* les `$\psi_i(\mathbf{r})$` forment une **base de fonctions complexes**

---

# Contexte

## Fonctions de base

Elles sont définies de la manière suivante : 

`$$
\psi_{m,n,n_z}(r_\perp, \theta, z)
        \equiv
    Z(z, n_z)
    .
    R(r_\perp, m, n)
    .
         e^{im\theta}
$$`

où
* `$
Z(z, n_z)
\equiv
    \frac{1}{\sqrt{b_z}}
    \frac{1}{\sqrt{2^{n_z} \sqrt{\pi}n_z!}}
    e^{-\frac{z^2}{2b_z^2}}H_{n_z}\left(\frac{z}{b_z}\right)
$`
* `$
R(r_\perp, m, n)
      \equiv
      \frac{1}{b_{\perp}\sqrt{\pi}}
      \sqrt{\frac{n!}{(n+|m|)!}}
      e^{-\frac{r_{\perp}^2}{2b_{\perp}^2}}
      \left(\frac{r_{\perp}}{b_{\perp}}\right)^{|m|}
      L_n^{|m|}\left(\frac{r_{\perp}^2}{b_{\perp}^2}\right).
$`

et 
* `$H_{n_z}$` le polynôme d'Hermite d'ordre `$n_z$`
* `$L_n^m$`  le polynôme de Laguerre d'ordre `$(n,m)$`

Chaque fonction est définie par un unique ensemble d'entiers `$(m,n,n_z)$`, appelés **nombres quantique** 

Pour avoir la même information entre `$\rho_{ab}$` et `$\rho(\mathbf{r})$`, la base doit être **infinie**


---

# Contexte

## Troncature de base

Dans la pratique, il est impossible de coder une base infinie. On utilise donc la **troncature de base**

En définissant : 
* `$
n_z^\textrm{max}(i) \equiv (N+2).Q^\frac{2}{3}+\frac{1}{2}-i.Q
$`
* `$
m^\textrm{max} \equiv \textrm{sup}\left\{i:n_z^\textrm{max}(i)\ge 1\right\}.
$`

avec les paramètres `$(N,Q)$` définis comme les **paramètres de la base de troncature**

On peut obtenir les plages de valeurs pour chaque nombre quantique : 

`$$
\begin{alignedat}{4}
0 & \le &&m  & \lt & \textrm{ mMax} \equiv m^\textrm{max}\\
0 & \le &&n  & \lt & \textrm{ nMax[m]} \equiv \frac{1}{2}(m^\textrm{max}-m-1) + 1\\
0 & \le &&n_z& \lt & \textrm{ n\_zMax[m, n]} \equiv n_z^\textrm{max}(m+2n+1).
\end{alignedat}
$$`

---

# Structure du projet

* Arborescence des fichiers : 
.alert.tree.hcenter[
Projet IPS-PROD
* bin/
    * tests
    * projet
* doc/
    * Documentation.pdf
* src/ 
    * basis.h
    * basis.cpp
    * poly.h
    * poly.ccp
    * main.cpp
* tests/
    * test_basis.h
    * test_poly.h
    * test_utils.h
* Doxyfile
* Makefile
* README.md
]

---


# Chaine de compilation - Extrait du Makefile

```Makefile
# Vars
CC = g++
SRCDIR  = src
LIBDIR  = obj
BINDIR  = bin
TESTDIR = tests

# Options du compilateur
CFLAGS = -O2 -Wall -Wextra -std=c++11 -g
LIBS   = -larmadillo

# Création des listes de fichiers utiles
SRC   = $(wildcard $(SRCDIR)/*.cpp)
HEADS = $(wildcard $(SRCDIR)/*.h)
OBJ   = $(SRC:$(SRCDIR)/%.cpp=$(LIBDIR)/%.o)
TEST  = $(wildcard $(TESTDIR)/test_*.h)

# Compilation du projet entier + librairie statique
projet: $(OBJ)
	ar rvs $(LIBDIR)/lib.a $(OBJ)
	$(CC) $(CFLAGS) -o $(BINDIR)/projet $(LIBDIR)/lib.a $(LIBS)

# Compilation en fichiers objet
$(LIBDIR)/%.o: $(SRCDIR)/%.cpp
	$(CC) $(CFLAGS) -o $@ -c $<

# Build et lance les tests unitaires
test:
	cxxtestgen --error-printer -o $(TESTDIR)/tests.cpp $(TEST)
	$(CC) $(CFLAGS) -o $(BINDIR)/tests $(TESTDIR)/tests.cpp $(LIBDIR)/lib.a
	./$(BINDIR)/tests

# Nettoie tous les fichiers générés
clean:
	rm -rf $(LIBDIR)/*.o
	rm -rf $(LIBDIR)/lib.a
	rm -rf $(BINDIR)/projet
	rm -rf $(BINDIR)/tests
	rm -rf $(TESTDIR)/tests.cpp
```

---

# Structuration du git


<canvas id='ips-prod'></canvas>

---

# Résultats - Tests Unitaires

.hcenter[
<asciinema-player src="data:application/json;base64,eyJ2ZXJzaW9uIjogMiwgIndpZHRoIjogODAsICJoZWlnaHQiOiAyNCwgInRpbWVzdGFtcCI6IDE2Mzk0MzMzMzAsICJlbnYiOiB7IlNIRUxMIjogIi9iaW4vYmFzaCIsICJURVJNIjogInh0ZXJtLTI1NmNvbG9yIn19ClswLjAyMzY4LCAibyIsICJcdTAwMWJdMDtjdGhpZW5hbkB0aGllbmFuLU1TSTogL21lZGlhL2N0aGllbmFuL0RBVEEvaXBzLXByb2QtcHJvamV0XHUwMDA3XHUwMDFiWzAxOzMybWN0aGllbmFuQHRoaWVuYW4tTVNJXHUwMDFiWzAwbTpcdTAwMWJbMDE7MzRtL21lZGlhL2N0aGllbmFuL0RBVEEvaXBzLXByb2QtcHJvamV0XHUwMDFiWzAwbSQgIl0KWzIuMzA5MzI0LCAibyIsICJtIl0KWzIuNDIxMTIzLCAibyIsICJhIl0KWzIuODA2OTY2LCAibyIsICJrIl0KWzIuOTEwNTA1LCAibyIsICJlIl0KWzMuNzg3MTczLCAibyIsICJcclxuIl0KWzMuNzg4ODU3LCAibyIsICJnKysgLVdhbGwgLVdleHRyYSAtc3RkPWMrKzExIC1nIC1vIG9iai9iYXNpcy5vIC1jIHNyYy9iYXNpcy5jcHBcclxuIl0KWzcuMzYyOTcsICJvIiwgImcrKyAtV2FsbCAtV2V4dHJhIC1zdGQ9YysrMTEgLWcgLW8gb2JqL3BvbHkubyAtYyBzcmMvcG9seS5jcHBcclxuIl0KWzEwLjY4NTE0OSwgIm8iLCAiZysrIC1XYWxsIC1XZXh0cmEgLXN0ZD1jKysxMSAtZyAtbyBvYmovbWFpbi5vIC1jIHNyYy9tYWluLmNwcFxyXG4iXQpbMTMuNDgwODc3LCAibyIsICJhciBydnMgb2JqL2xpYi5hIG9iai9iYXNpcy5vIG9iai9wb2x5Lm8gb2JqL21haW4ub1xyXG4iXQpbMTMuNDgzMjA4LCAibyIsICJhcjogIl0KWzEzLjQ4MzQ4NiwgIm8iLCAiY3LDqWF0aW9uIGRlIG9iai9saWIuYVxyXG4iXQpbMTMuNDgzNTc1LCAibyIsICJhIC0gb2JqL2Jhc2lzLm9cclxuYSAtIG9iai9wb2x5Lm9cclxuYSAtIG9iai9tYWluLm9cclxuIl0KWzEzLjQ5MTk3NiwgIm8iLCAiZysrIC1XYWxsIC1XZXh0cmEgLXN0ZD1jKysxMSAtZyAtbyBiaW4vcHJvamV0IG9iai9saWIuYSAtbGFybWFkaWxsb1xyXG4iXQpbMTMuNTYzMzM0LCAibyIsICJcdTAwMWJdMDtjdGhpZW5hbkB0aGllbmFuLU1TSTogL21lZGlhL2N0aGllbmFuL0RBVEEvaXBzLXByb2QtcHJvamV0XHUwMDA3XHUwMDFiWzAxOzMybWN0aGllbmFuQHRoaWVuYW4tTVNJXHUwMDFiWzAwbTpcdTAwMWJbMDE7MzRtL21lZGlhL2N0aGllbmFuL0RBVEEvaXBzLXByb2QtcHJvamV0XHUwMDFiWzAwbSQgIl0KWzE2LjU3MDI4NCwgIm8iLCAibSJdClsxNi42Mzc4OTYsICJvIiwgImEiXQpbMTYuNzc1MDkyLCAibyIsICJrIl0KWzE2Ljg1MTUxMiwgIm8iLCAiZSJdClsxNi45NDYxMjYsICJvIiwgIiAiXQpbMTcuMTQ3ODI1LCAibyIsICJ0Il0KWzE3LjMxMzY4MywgIm8iLCAiZSJdClsxNy40ODAzNDIsICJvIiwgInMiXQpbMTcuNjk3MjgyLCAibyIsICJ0Il0KWzE4LjM0NTUzNywgIm8iLCAiXHJcbiJdClsxOC4zNDg5MTQsICJvIiwgImN4eHRlc3RnZW4gLS1lcnJvci1wcmludGVyIC1vIHRlc3RzL3Rlc3RzLmNwcCB0ZXN0cy90ZXN0X3BvbHkuaCB0ZXN0cy90ZXN0X2Jhc2lzLmhcclxuIl0KWzE4LjQyMTc3NywgIm8iLCAiZysrIC1XYWxsIC1XZXh0cmEgLXN0ZD1jKysxMSAtZyAtbyBiaW4vdGVzdHMgdGVzdHMvdGVzdHMuY3BwIG9iai9saWIuYVxyXG4iXQpbMjIuMjI2MjksICJvIiwgIi4vYmluL3Rlc3RzXHJcbiJdClsyMi4yMjc0ODQsICJvIiwgIlJ1bm5pbmcgY3h4dGVzdCB0ZXN0cyAoNCB0ZXN0cykiXQpbMjIuMjI3NTIzLCAibyIsICIuIl0KWzIyLjIyNzYwOSwgIm8iLCAiLi4iXQpbMjIuMjI3NjU2LCAibyIsICIuT0shXHJcbiJdClsyMi4yMjgxMywgIm8iLCAiXHUwMDFiXTA7Y3RoaWVuYW5AdGhpZW5hbi1NU0k6IC9tZWRpYS9jdGhpZW5hbi9EQVRBL2lwcy1wcm9kLXByb2pldFx1MDAwN1x1MDAxYlswMTszMm1jdGhpZW5hbkB0aGllbmFuLU1TSVx1MDAxYlswMG06XHUwMDFiWzAxOzM0bS9tZWRpYS9jdGhpZW5hbi9EQVRBL2lwcy1wcm9kLXByb2pldFx1MDAxYlswMG0kICJd"></asciinema-player>
]

---

# Optimisation - Algorithme naïf

```C++
// Le copier-coller, c'est le mal

for (int m1 = 0; m1 < basis.mMax; m1++)
{
    for (int n1 = 0; n1 < basis.nMax(m1); n1++)
    {
        for (int n_z1 = 0; n_z1 < basis.n_zMax(m1, n1); n_z1++)
        {
            j = 0;
            for (int m2 = 0; m2 < basis.mMax; m2++)
            {
                for (int n2 = 0; n2 < basis.nMax(m2); n2++)
                {
                    for (int n_z2 = 0; n_z2 < basis.n_zMax(m2, n2); n_z2++)
                    {
                        result += (basis.rPart(rVals, m1, n1) * basis.zPart(zVals, n_z1).t())
                                % (basis.rPart(rVals, m2, n2) * basis.zPart(zVals, n_z2).t())
                                * rho(i,j);
                        j++;
                    }
                }
            }
            i++;
        }
    }
}

```

---

# Optimisation - `$ m1 = m2 = m $`

```C++
// une boucle en moins et calculs le plus tôt possible

for (int m = 0; m < basis.mMax; m++)
{
    for (int n1 = 0; n1 < basis.nMax(m); n1++)
    {
        rPartA = basis.rPart(rVals, m, n1);
        for (int n_z1 = 0; n_z1 < basis.n_zMax(m, n1); n_z1++)
        {
            zPartA = basis.zPart(zVals, n_z1);
            funcA = rPartA * zPartA.t();
            for (int n2 = 0; n2 < basis.nMax(m); n2++)
            {
                rPartB = basis.rPart(rVals, m, n2);
                for (int n_z2 = 0; n_z2 < basis.n_zMax(m, n2); n_z2++)
                {   
                    zPartB = basis.zPart(zVals, n_z2);
                    funcB = rPartB * zPartB.t();

                    result += funcA % funcB * rho(i,j);
                    ib++;
                }
            }
            i++;
            if (i == j)
                inc = i;
            j = inc;
        }
    }
}
```

---

# Optimisation - Symmétrie sur l'axe Z

```C++
// taille divisée par deux

arma::mat result3   = arma::zeros(nbR/2, nbZ);
arma::mat funcASym  = arma::zeros(nbR/2, nbZ);
arma::mat funcBSym  = arma::zeros(nbR/2, nbZ);
arma::vec rPartASym = arma::zeros(nbR/2);
arma::vec rPartBSym = arma::zeros(nbR/2);

for (int m = 0; m < basis.mMax; m++)
{
    for (int n = 0; n < basis.nMax(m); n++)
    {
        rPartASym = basis.rPart(rValsSym, m, n);
        for (int n_z = 0; n_z < basis.n_zMax(m, n); n_z++)
        {
            zPartA = basis.zPart(zVals, n_z);
            funcA = rPartASym * zPartA.t();
            for (int np = 0; np < basis.nMax(m); np++)
            {
                rPartBSym = basis.rPart(rValsSym, m, np);
                for (int n_zp = 0; n_zp < basis.n_zMax(m, np); n_zp++)
                {   
                    zPartB = basis.zPart(zVals, n_zp);
                    funcB = rPartBSym * zPartB.t();

                    result3 += funcA % funcB * rho(ia,ib);
                    ib++;
                }
            }
            ia++;
            if (ia == ib)
                inc = ia;
            ib = inc;
        }
    }
}
```

---

# Résultats - Speedups

.hcenter[
<asciinema-player src="data:application/json;base64,
eyJ2ZXJzaW9uIjogMiwgIndpZHRoIjogODAsICJoZWlnaHQiOiAyNCwgInRpbWVzdGFtcCI6IDE2Mzk0Nzc5NTEsICJlbnYiOiB7IlNIRUxMIjogIi9iaW4vYmFzaCIsICJURVJNIjogInh0ZXJtLTI1NmNvbG9yIn19ClswLjAxMTAwMSwgIm8iLCAiXHUwMDFiWz8yMDA0aGxhcHRvcD4gIl0KWzEuMTIyMjYyLCAibyIsICIuIl0KWzEuMTk5NDIxLCAibyIsICIvIl0KWzIuMjI2NjA4LCAibyIsICJiIl0KWzIuMzc3MDU0LCAibyIsICJpIl0KWzIuNDcwMTM1LCAibyIsICJuIl0KWzIuODM1MjE2LCAibyIsICIvIl0KWzMuMTM3MDQzLCAibyIsICJwIl0KWzMuMjM4Mzc1LCAibyIsICJyIl0KWzMuMzIwNzMzLCAibyIsICJvIl0KWzMuNDM2MjM5LCAibyIsICJqZXQgIl0KWzQuMDk2MTg5LCAibyIsICIwIl0KWzQuNjk3Mzk0LCAibyIsICJcclxuXHUwMDFiWz8yMDA0bFxyIl0KWzYuMzQxNzA3LCAibyIsICJcclxuQWxnb3JpdGhtZSBuYcOvZiA6IDE0OTQuODE5Mjc4IG1zXHJcblxyXG5tMSA9IG0yID0gbSAgICAgOiA3OS4zMzQxMjQgbXNcclxuICAgLSBTcGVlZHVwIDogMTguODQyMDcyXHJcblxyXG5TeW1tw6l0cmllIHN1ciBYIDogNTQuMzk2MDIwIG1zXHJcbiAgIC0gU3BlZWR1cCA6IDEuNDU4NDU1XHJcbiAgIC0gQ3VtdWzDqSAgOiAyNy40ODAzMDZcclxuXHJcbiJdCls2LjM0MjEzOSwgIm8iLCAiXHUwMDFiWz8yMDA0aGxhcHRvcD4gIl0"></asciinema-player>
]

---

# Résultats - Plots

.row[
.column.w50.middle[
.hcenter.shadow[![](images/figure2D.png)]
.hcenter[Rendu 2D de la matrice `$\rho(\mathbf{r})$`]
]

.column.w50.middle[
.hcenter.shadow[![](images/render.png)]
.hcenter[Rendu 3D de la matrice `$\rho(\mathbf{r})$`]
]
]

    </textarea>

    <script src="core/javascript/remark.js"></script>
    <script src="core/javascript/katex.min.js"></script>
    <script src="core/javascript/auto-render.min.js"></script>
    <script src="core/javascript/emojify.js"></script>
    <script src="core/javascript/mermaid.js"></script>
    <script src="core/javascript/jquery-2.1.1.min.js"></script>
    <script src="core/javascript/extend-jquery.js"></script>
    <script src="core/javascript/gitgraph.js"></script>
    <script src="core/javascript/plotly.js"></script>
    <script src="core/javascript/asciinema-player.js"></script>
    <script src="core/javascript/bokeh-2.2.1.min.js"></script>
    <script src="core/javascript/bokeh-widgets-2.2.1.min.js"></script>
    <script src="core/javascript/bokeh-tables-2.2.1.min.js"></script>
    <script src="core/javascript/bokeh-api-2.2.1.min.js"></script>

    <script>

    // === Remark.js initialization ===
    var slideshow = remark.create(
    {
      highlightStyle: 'monokai',
      countIncrementalSlides: false,
      highlightLines: false
    });

    // === Mermaid.js initialization ===
    mermaid.initialize({
      startOnLoad: false,
      cloneCssStyles: false,
      flowchart:{
        height: 50
      },
      sequenceDiagram:{
        width: 110,
        height: 30
      }
    });

    function initMermaid(s) {
      var diagrams = document.querySelectorAll('.mermaid');
      var i;
      for(i=0;i<diagrams.length;i++){
        if(diagrams[i].offsetWidth>0){
          mermaid.init(undefined, diagrams[i]);
        }
      }
    }

    slideshow.on('afterShowSlide', initMermaid);
    initMermaid(slideshow.getSlides()[slideshow.getCurrentSlideIndex()]);

    
    // === Emojify.js initialization ===
    emojify.run();

    // KaTeX
    renderMathInElement(document.body,{delimiters: [{left: "$$", right: "$$", display: true}, {left: "$", right: "$", display: false}], ignoredTags: ["script", "noscript", "style", "textarea", "pre"] });

    // === Cinescript initialization ===
    $(document).ready(init_cinescripts);

    // ===== END =====

    </script>
    <script src="gitgraphs.js" type="text/javascript"></script>
  </body>
</html>

