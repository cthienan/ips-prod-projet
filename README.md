# IPS-PROD Projet

Projet d'Introduction à la Programmation Scientifique - PROD


## Auteurs

Thien-An CAO, Mathis MARTIN


## Sujet

Le projet consiste à calculer et plotter l'état d'un système nucléaire donné à
l'aide du C++ et de la librairie Armadillo.


## Utilisation

### Dépendances

- make
- gcc/g++
- (armadillo)[http://arma.sourceforge.net/], librairie pour le calcul scientifique basée sur BLAS
- (cxxtest)[https://github.com/CxxTest/cxxtest], librairie de tests pour C++
- (povray)[https://github.com/POV-Ray/povray], ray-tracer
- (doxygen)[https://www.doxygen.nl/index.html], pour produire la documentation du projet

### Documentation

La documentation est compilable via la commande `doxygen`

### Makefile

`make` pour compiler le projet et les tests

`make test` pour compiler et lancer les tests unitaires

`make clean` pour tout nettoyer

### Le progamme

Le binaire compilé se trouve dans le répertoire `bin`, mais il ne faut pas
l'exécuter directement, sous peine d'être accueilli par une chaîne de caractère
incompréhensible au formal df3.

Il faut se placer à la racine du projet et lancer le script suivant :

`./run.sh $WIDTH $HEIGHT`

qui va exécuter le programme, stocker le résultat du calcul dans
`data/result.df3` et lancer le rendu 3D du graphe de la fonction de densité
par POV-RAY pour une image aux dimensions `WIDTH x HEIGHT`.